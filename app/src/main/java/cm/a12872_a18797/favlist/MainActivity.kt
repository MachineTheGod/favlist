package cm.a12872_a18797.favlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import cm.a12872_a18797.favlist.fragments.HomeFragment
import cm.a12872_a18797.favlist.fragments.FavoritesFragment
import cm.a12872_a18797.favlist.fragments.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //region Bottom Navigation Menu =================================

        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottom_navigation)

        val homeFragment = HomeFragment()
        val favouritesFragment = FavoritesFragment()
        val profileFragment = ProfileFragment()
        setCurrentFragment(homeFragment)

        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_home -> {
                    setCurrentFragment(homeFragment)
                    Log.i(TAG, "Home Selected")
                }
                R.id.nav_lists -> {
                    setCurrentFragment(favouritesFragment)
                    Log.i(TAG, "Favorites Selected")
                }
                R.id.nav_search -> {
                    setCurrentFragment(profileFragment)
                    Log.i(TAG, "Profile Selected")
                }
            }
            true
        }

        //endregion
    }

    private fun setCurrentFragment(fragment : Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }
}
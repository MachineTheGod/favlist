package cm.a12872_a18797.favlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import cm.a12872_a18797.favlist.fragments.HomeFragment
import cm.a12872_a18797.favlist.fragments.FavoritesFragment
import cm.a12872_a18797.favlist.fragments.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class ImageDetailActivity : AppCompatActivity() {

    val TAG = "ImageDetailActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_detail)

        //region Bottom Navigation Menu =====================================

        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottom_navigation)

        val homeFragment = HomeFragment()
        val listsFragment = FavoritesFragment()
        val searchFragment = ProfileFragment()

        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_home -> {
                    setCurrentFragment(homeFragment)
                    Log.i(TAG, "Home Selected")
                }
                R.id.nav_lists -> {
                    setCurrentFragment(listsFragment)
                    Log.i(TAG, "My Lists Selected")
                }
                R.id.nav_search -> {
                    setCurrentFragment(searchFragment)
                    Log.i(TAG, "Search Selected")
                }
            }
            true
        }

        //endregion ==

        val detailText = findViewById<TextView>(R.id.detailText)
        val detailImage = findViewById<ImageView>(R.id.detailImage)

        var modalItems: Model = intent.getSerializableExtra("data") as Model

        detailText.text = modalItems.name
        detailImage.setImageResource(modalItems.image!!)
    }

    private fun setCurrentFragment(fragment : Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }
}
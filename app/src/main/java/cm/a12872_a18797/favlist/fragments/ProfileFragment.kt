package cm.a12872_a18797.favlist.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cm.a12872_a18797.favlist.ImageDetailActivity
import cm.a12872_a18797.favlist.LoginActivity
import cm.a12872_a18797.favlist.Model
import cm.a12872_a18797.favlist.R
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment : Fragment() {

    lateinit var rootView: View

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false)

        val userEmail = rootView.findViewById<TextView>(R.id.log_name)
        val userId = rootView.findViewById<TextView>(R.id.log_uid)
        val logoutButton = rootView.findViewById<Button>(R.id.logout_button)

        userEmail.text = FirebaseAuth.getInstance().currentUser!!.email.toString()
        userId.text = FirebaseAuth.getInstance().currentUser!!.uid

        logoutButton.setOnClickListener{
            FirebaseAuth.getInstance().signOut()

            Toast.makeText(
                    this@ProfileFragment.requireContext(),
                    "Your Session has Been Terminated. You're Logged Out.",
                    Toast.LENGTH_SHORT
            ).show()

            val intent = Intent(this@ProfileFragment.requireContext(), LoginActivity::class.java)
            startActivity(intent)
        }

        return rootView
    }

}

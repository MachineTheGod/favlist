package cm.a12872_a18797.favlist.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import cm.a12872_a18797.favlist.ImageDetailActivity
import cm.a12872_a18797.favlist.Model
import cm.a12872_a18797.favlist.R
import org.w3c.dom.Text

class FavoritesFragment : Fragment() {

    lateinit var rootView: View

    var favouritesList : MutableList<Model> = ArrayList()
    lateinit var listViewFav : ListView
    lateinit var adapter: ListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_favorites, container, false)

        favouritesList.add(Model("Golden Bridge", R.drawable.bridge))
        favouritesList.add(Model("Tampa Bay Buccaneers", R.drawable.buccaneers))
        favouritesList.add(Model("Sweet and Sour Chicken", R.drawable.chicken))
        favouritesList.add(Model("Sidney's Opera House", R.drawable.operahouse))
        favouritesList.add(Model("Warm Salad Dish", R.drawable.warmsalad))

        listViewFav = rootView.findViewById(R.id.favourites_list)
        adapter = ListAdapter()
        listViewFav.adapter = adapter

        return rootView
    }

    inner class ListAdapter : BaseAdapter(){

        override fun getCount(): Int {
            return favouritesList.size
        }

        override fun getItem(position: Int): Any {
            return favouritesList[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.list_item, parent, false)
            val imageView = rowView.findViewById<ImageView>(R.id.list_item_image)
            val textViewName = rowView.findViewById<TextView>(R.id.list_item_text)

            favouritesList[position].image?.let { imageView.setImageResource(it) }
            textViewName.text = favouritesList[position].name

            rowView.isClickable = true
            rowView.setOnClickListener{
                var intent = Intent(this@FavoritesFragment.requireContext(), ImageDetailActivity::class.java)
                intent.putExtra("data", favouritesList[position])
                startActivity(intent)
            }

            return rowView
        }
    }

}

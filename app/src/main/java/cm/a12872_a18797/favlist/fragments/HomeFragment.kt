package cm.a12872_a18797.favlist.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import cm.a12872_a18797.favlist.ImageDetailActivity
import cm.a12872_a18797.favlist.Model
import cm.a12872_a18797.favlist.R
import com.google.firebase.database.FirebaseDatabase

class HomeFragment : Fragment() {

    lateinit var rootView: View

    public val modalList = ArrayList<Model>()

    lateinit var gridView: GridView

    private var descriptions = arrayOf(
        "Golden Bridge", "Tampa Bay Buccaneers", "Chevrolet Camaro",
        "Vegetable Casserole", "Sweet and Sour Chicken", "Design Vector",
        "Pork Loin", "Sidney's Opera House", "Pasta Dish",
        "Dope Setup", "Singapore", "Tacos Dish",
        "Toquio", "Warm Salad Dish"
    )
    private var images = intArrayOf(
        R.drawable.bridge, R.drawable.buccaneers, R.drawable.camaro,
        R.drawable.casserole, R.drawable.chicken, R.drawable.design,
        R.drawable.lombo, R.drawable.operahouse, R.drawable.pasta,
        R.drawable.setup, R.drawable.singapore, R.drawable.tacos,
        R.drawable.toquio, R.drawable.warmsalad
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_home, container, false)

        //region Firebase Handling ===========================

        var database = FirebaseDatabase.getInstance().reference
        database.setValue("Test Value")

        //endregion

        gridView = rootView.findViewById(R.id.home_grid)

        for (i in descriptions.indices){
            modalList.add(Model(descriptions[i], images[i]))
        }

        var customAdapter = GridAdapter(modalList, this@HomeFragment.requireContext())
        gridView.adapter = customAdapter

        gridView.setOnItemClickListener() { adapterView, view, position, l ->
            var intent = Intent(this@HomeFragment.requireContext(), ImageDetailActivity::class.java)
            intent.putExtra("data", modalList[position])
            startActivity(intent)
        }

        return rootView
    }

    class GridAdapter(var itemModel : ArrayList<Model>, var context: Context): BaseAdapter(){

        var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView

            if (view == null){
                view = layoutInflater.inflate(R.layout.home_grid_row, parent, false)
            }

            var imageViewName = view?.findViewById<ImageView>(R.id.grid_image)
            imageViewName?.setImageResource(itemModel[position].image!!)

            return view!!
        }

        override fun getItem(position: Int): Any {
            return itemModel[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return itemModel.size
        }

    }
}

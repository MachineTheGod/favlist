package cm.a12872_a18797.favlist

import java.io.Serializable

class Model : Serializable {

    var name : String? = null
    var image : Int? = null

    constructor(name: String?, image: Int?) {
        this.name = name
        this.image = image
    }
}